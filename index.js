// Retrieve all the to do list items
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(json => console.log(json))

// Return the title only
fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(json => {
    const items = json.map(item => item.title);
    console.log(items);
  })


// Retrieve single to do list item
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(response => response.json())
.then(json => console.log(json))

// Create a to do list  item
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Added to do list items'
	})
})
.then(response => response.json())
.then(json => console.log(json))

// Update a to do list  item
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated items'
	})
})
.then(response => response.json())
.then(json => console.log(json))

// Update a to do list structure
fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Lorem Ipsum',
		description: 'To be the very best',
		status: 'Not yet completed',
		dateCompleted: '04/27/2023',
		userID: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))

// Update a to do list item using PATCH
fetch('https://jsonplaceholder.typicode.com/todos/3', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated items using PATCH'
	})
})
.then(response => response.json())
.then(json => console.log(json))

// Changing the status and add a date of modification
fetch('https://jsonplaceholder.typicode.com/todos/4', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: true,
		dateModified: '04/27/2023'
	})
})
.then(response => response.json())
.then(json => console.log(json))

// Delete an item
fetch('https://jsonplaceholder.typicode.com/todos/5', {
	method: 'DELETE'
})
.then(response => response.json())
.then(json => console.log(json))